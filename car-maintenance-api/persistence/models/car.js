const { DataTypes } = require('sequelize');

const dayjs = require('../../util/dayjs');

const DAYS_IN_YEAR = 365;
// based on ~11k annual mileage
const DEFAULT_DAILY_MILEAGE = 30;

const CarModel = sequelizeInstance => {
    return sequelizeInstance.define('cars', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        make: {
            type: DataTypes.STRING,
            allowNull: false
        },
        model: {
            type: DataTypes.STRING,
            allowNull: false
        },
        year: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        color: {
            type: DataTypes.STRING,
            allowNull: false
        },
        nickname: {
            type: DataTypes.STRING
        },
        miles: {
            type: DataTypes.INTEGER
        },
        mileage: {
            type: DataTypes.INTEGER
        },
        ownerId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'owner_id',
            references: {
                model: 'users',
                key: 'id'
            }
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: false,
            field: 'updated_at'
        },
        estimatedDailyMileage: {
            type: DataTypes.VIRTUAL,
            get() {
                // if user provides yearly mileage, use that to get daily mileage
                if (this.mileage) {
                    return Math.round(this.mileage / DAYS_IN_YEAR);
                }

                // Otherwise calculate it based on total miles driven
                const lastUpdated = dayjs(this.updatedAt).utc();
                const beginning = dayjs([this.year, 7]);
                const daysDifference = Math.round(
                    dayjs.duration(lastUpdated.diff(beginning)).asDays()
                );
                const dailyMileage = Math.round(this.miles / daysDifference);
                // in the event daily mileage is 0 or below, use the default value (for example, buying a year 2022 car in 2021)
                return dailyMileage > 0 ? dailyMileage : DEFAULT_DAILY_MILEAGE;
            }
        },
        estimatedYearlyMileage: {
            type: DataTypes.VIRTUAL,
            get() {
                return this.mileage
                    ? this.mileage
                    : this.estimatedDailyMileage * DAYS_IN_YEAR;
            }
        },
        estimatedMiles: {
            type: DataTypes.VIRTUAL,
            get() {
                const lastUpdated = dayjs(this.updatedAt).utc();
                const now = dayjs().utc();
                const diffSinceUpdated = dayjs
                    .duration(now.diff(lastUpdated))
                    .asDays();

                if (diffSinceUpdated >= 1) {
                    const daysSinceUpdated = Math.round(diffSinceUpdated);
                    return (
                        Math.round(
                            daysSinceUpdated * this.estimatedDailyMileage
                        ) + this.miles
                    );
                }
                return null;
            }
        },
        // handle the simple check of which mileage field to use for maintenance comparison here in one central location
        maintenanceMileage: {
            type: DataTypes.VIRTUAL,
            get() {
                return this.estimatedMiles ? this.estimatedMiles : this.miles;
            }
        }
    });
};

module.exports = CarModel;
