export const setAuth = auth => ({
    type: 'SET_AUTH',
    auth
});

export const clearAuth = () => ({
    type: 'CLEAR_AUTH'
});

export const setUserExists = userId => ({
    type: 'SET_USER_EXISTS',
    userId
});

export const setUserAdmin = isAdmin => ({
    type: 'SET_USER_ADMIN',
    isAdmin
});
