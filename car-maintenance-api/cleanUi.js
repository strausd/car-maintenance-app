const fs = require('fs-extra');

const apiPublic = './public';
const uiDist = '../car-maintenance-ui/public/dist';

fs.removeSync(apiPublic);
fs.removeSync(uiDist);

console.log('UI code removed');
