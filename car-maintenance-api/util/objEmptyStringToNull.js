module.exports = obj => {
    // Clone object
    const newObj = { ...obj };
    // Iterate over all object properties
    Object.keys(newObj).forEach(key => {
        // If value is an empty string, set it to null
        if (newObj[key] === '') {
            newObj[key] = null;
        }
    });
    // Return modified object
    return newObj;
};
