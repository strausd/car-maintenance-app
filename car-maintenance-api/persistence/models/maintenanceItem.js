const { DataTypes } = require('sequelize');

const MaintenanceItemModel = sequelizeInstance => {
    return sequelizeInstance.define('maintenance_items', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: {
            type: DataTypes.STRING,
            allowNull: false
        },
        description: {
            type: DataTypes.STRING,
            allowNull: true
        },
        targetDate: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'target_date'
        },
        targetMiles: {
            type: DataTypes.INTEGER,
            allowNull: true,
            field: 'target_miles'
        },
        completedDate: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'completed_date'
        },
        completedMiles: {
            type: DataTypes.INTEGER,
            allowNull: true,
            field: 'completed_miles'
        },
        isComplete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            field: 'is_complete'
        },
        carId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'car_id',
            references: {
                model: 'cars',
                key: 'id'
            }
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: false,
            field: 'updated_at'
        }
    });
};

module.exports = MaintenanceItemModel;
