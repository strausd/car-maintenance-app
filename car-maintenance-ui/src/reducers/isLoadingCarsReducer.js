const defaultState = false;

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'SET_IS_LOADING_CARS':
            return action.isLoadingCars;
        default:
            return state;
    }
};
