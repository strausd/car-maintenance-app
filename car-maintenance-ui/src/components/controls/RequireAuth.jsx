import React from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { Transition } from 'react-transition-group';

import HomeLogin from './HomeLogin';

const duration = 500;
const defaultStyle = {
    transition: `opacity ${duration}ms ease-in-out`,
    opacity: 0
};
const transitionStyles = {
    entering: { opacity: 1 },
    entered: { opacity: 1 },
    exiting: { opacity: 0 },
    exited: { opacity: 0 }
};

const RequireAuth = ({
    render,
    requireAdmin,
    isAuthenticated,
    isAdmin,
    ...restProps
}) => {
    const shouldRender =
        isAuthenticated && (!requireAdmin || (requireAdmin && isAdmin));
    const RenderComponent = render;
    return (
        <>
            <Transition in={shouldRender} timeout={duration}>
                {state => (
                    <div
                        style={{ ...defaultStyle, ...transitionStyles[state] }}
                    >
                        <RenderComponent {...restProps} />
                    </div>
                )}
            </Transition>
            <Transition in={!shouldRender} timeout={duration}>
                {state => (
                    <div
                        className="page home"
                        style={{ ...defaultStyle, ...transitionStyles[state] }}
                    >
                        <HomeLogin />
                    </div>
                )}
            </Transition>
        </>
    );
    // if the component requires admin and the user is an admin, OR if the component doesn't require admin at all, render it if they are authenticated
    // if (isAuthenticated && (!requireAdmin || (requireAdmin && isAdmin))) {
    //     const RenderComponent = render;
    //     return <RenderComponent {...restProps} />;
    // }
    // return (

    //     <div className="page home">
    //         <HomeLogin />
    //     </div>
    // );
};

RequireAuth.propTypes = {
    render: propTypes.func,
    requireAdmin: propTypes.bool,
    isAuthenticated: propTypes.bool,
    isAdmin: propTypes.bool
};

RequireAuth.defaultProps = {
    requireAdmin: false
};

const mapStateToProps = ({ auth }) => ({
    isAuthenticated: !!auth.accessToken && auth.userExists,
    isAdmin: auth.profile.isAdmin
});

export default connect(mapStateToProps)(RequireAuth);
