const { CarModel } = require('../persistence/sequelize');

module.exports = (req, res, next) => {
    // If admin, continue
    if (res.locals.user.isAdmin) {
        return next();
    }
    CarModel.findOne({
        where: {
            id: req.params.id
        },
        include: ['sharedCars']
    })
        .then(car => {
            const isSharedUrl = req.url.includes('share');
            const carObj = car.get({ plain: true });
            // If a car is returned, we check the owner
            if (car) {
                // check car based on owner
                if (carObj.ownerId === res.locals.user.id) {
                    return next();
                } else if (
                    !isSharedUrl &&
                    !!carObj.sharedCars.find(
                        shared => shared.email === res.locals.user.email
                    )
                ) {
                    return next();
                } else {
                    return res.status(403).json({ msg: 'Unauthorized' });
                }
            }
            // If no car is returned, we send back a 404
            else {
                return res.status(404).json({
                    msg: `No car found for id ${req.params.id}`
                });
            }
        })
        .catch(e => {
            console.error(e);
            res.status(500).json({ msg: 'Uh oh. We ran into an error :(' });
        });
};
