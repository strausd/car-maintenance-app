const express = require('express');

const maintenanceService = require('../service/maintenanceService');

const router = express.Router();

router.get('/car/:id/maintenance', (req, res) => {
    maintenanceService.getCarMaintenanceItems(req, res);
});
router.post('/car/:id/maintenance', (req, res) => {
    maintenanceService.createMaintenanceItem(req, res);
});
router.get('/car/:id/maintenance/:m_id', (req, res) => {
    maintenanceService.getMaintenanceItem(req, res);
});
router.patch('/car/:id/maintenance/:m_id', (req, res) => {
    maintenanceService.updateMaintenanceItem(req, res);
});
router.delete('/car/:id/maintenance/:m_id', (req, res) => {
    maintenanceService.deleteMaintenanceItem(req, res);
});

module.exports = router;
