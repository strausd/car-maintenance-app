import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Grid, Button, Icon } from 'semantic-ui-react';
import { reverse } from 'named-urls';

import { addCommasToNumbers } from '../../misc/util';
import EditCarModal from './EditCarModal';
import SharedUsersModal from './SharedUsersModal';
import routes from '../../routers/routes';

class CarDetailsTable extends React.Component {
    static propTypes = {
        history: PropTypes.object,
        car: PropTypes.object,
        showDetailsButton: PropTypes.bool,
        userId: PropTypes.number
    };

    static defaultProps = {
        showDetailsButton: false
    };

    constructor(props) {
        super(props);
        this.state = {
            isEditModalOpen: false,
            isSharedUsersModalOpen: false
        };
    }

    toggleEditModal = () =>
        this.setState(prevState => ({
            isEditModalOpen: !prevState.isEditModalOpen
        }));

    toggleSharedUsersModal = () =>
        this.setState(prevState => ({
            isSharedUsersModalOpen: !prevState.isSharedUsersModalOpen
        }));

    getMilesText = () => {
        if (this.props.car.estimatedMiles) {
            return `Estimated ${addCommasToNumbers(
                this.props.car.estimatedMiles
            )} (Last actual ${addCommasToNumbers(this.props.car.miles)})`;
        }
        return addCommasToNumbers(this.props.car.miles);
    };

    getMileageText = () => {
        if (this.props.car.mileage) {
            return addCommasToNumbers(this.props.car.mileage);
        }
        return `Estimated ${addCommasToNumbers(
            this.props.car.estimatedYearlyMileage
        )}`;
    };

    handleDetailsRedirect = () =>
        this.props.history.push(
            reverse(routes.carDetail, { id: this.props.car.id })
        );

    canManageSharedUsers = () => {
        return (
            this.props.userId === this.props.car.ownerId &&
            !this.props.showDetailsButton
        );
    };

    render() {
        return (
            <>
                <Grid.Row>
                    <Grid.Column
                        tablet={6}
                        computer={6}
                        largeScreen={6}
                        mobile={8}
                    >
                        <span className="bold">Year: </span>
                        {this.props.car.year}
                    </Grid.Column>
                    <Grid.Column
                        tablet={6}
                        computer={6}
                        largeScreen={6}
                        mobile={8}
                    >
                        <span className="bold">Make: </span>
                        {this.props.car.make}
                    </Grid.Column>
                    <Grid.Column
                        tablet={4}
                        computer={4}
                        largeScreen={4}
                        mobile={8}
                    >
                        <span className="bold">Model: </span>
                        {this.props.car.model}
                    </Grid.Column>
                    <Grid.Column
                        tablet={6}
                        computer={6}
                        largeScreen={6}
                        mobile={8}
                    >
                        <span className="bold">Nickname: </span>
                        {this.props.car.nickname}
                    </Grid.Column>
                    <Grid.Column
                        tablet={6}
                        computer={6}
                        largeScreen={6}
                        mobile={8}
                    >
                        <span className="bold">Color: </span>
                        {this.props.car.color}
                    </Grid.Column>
                    <Grid.Column
                        tablet={4}
                        computer={4}
                        largeScreen={4}
                        mobile={8}
                    >
                        <span className="bold">Miles: </span>
                        {this.getMilesText()}
                    </Grid.Column>
                    <Grid.Column
                        tablet={4}
                        computer={4}
                        largeScreen={4}
                        mobile={8}
                    >
                        <span className="bold">Yearly Mileage: </span>
                        {this.getMileageText()}
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row centered className="stacked-buttons">
                    {this.canManageSharedUsers() && (
                        <Grid.Column computer={6} tablet={8} mobile={16}>
                            <Button
                                fluid
                                animated="fade"
                                color="black"
                                className="round-button"
                                onClick={this.toggleSharedUsersModal}
                            >
                                <Button.Content visible>
                                    Manage shared users
                                </Button.Content>
                                <Button.Content hidden>
                                    <Icon name="users" />
                                </Button.Content>
                            </Button>
                        </Grid.Column>
                    )}
                    {this.props.showDetailsButton && (
                        <Grid.Column computer={6} tablet={8} mobile={16}>
                            <Button
                                fluid
                                animated="fade"
                                color="black"
                                className="round-button"
                                onClick={this.handleDetailsRedirect}
                            >
                                <Button.Content visible>
                                    See car details
                                </Button.Content>
                                <Button.Content hidden>
                                    <Icon name="unordered list" />
                                </Button.Content>
                            </Button>
                        </Grid.Column>
                    )}
                    <Grid.Column computer={6} tablet={8} mobile={16}>
                        <Button
                            fluid
                            animated="fade"
                            color="black"
                            className="round-button"
                            onClick={this.toggleEditModal}
                        >
                            <Button.Content visible>
                                Edit this car
                            </Button.Content>
                            <Button.Content hidden>
                                <Icon name="edit" />
                            </Button.Content>
                        </Button>
                    </Grid.Column>
                </Grid.Row>
                <EditCarModal
                    open={this.state.isEditModalOpen}
                    toggleModal={this.toggleEditModal}
                    car={this.props.car}
                />
                <SharedUsersModal
                    open={this.state.isSharedUsersModalOpen}
                    toggleModal={this.toggleSharedUsersModal}
                    sharedUsers={this.props.car.sharedCars}
                />
            </>
        );
    }
}

const mapStateToProps = ({ auth }) => ({
    userId: auth.userId
});

export default connect(mapStateToProps)(withRouter(CarDetailsTable));
