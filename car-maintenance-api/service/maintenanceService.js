const { MaintenanceItemModel, CarModel } = require('../persistence/sequelize');
const { validateDateTime, getDateTimeString } = require('../util/time');
const {
    organizeCarMaintenanceItems,
    getMaintenanceItemPriority
} = require('../util/organizeCarMaintenanceItems');

const getCarMaintenanceItems = (req, res) => {
    CarModel.findOne({
        where: {
            id: req.params.id
        },
        include: 'maintenance_items'
    })
        .then(car => {
            const {
                pastMaintenance,
                futureMaintenance
            } = organizeCarMaintenanceItems(car);
            res.status(200).json({
                pastMaintenance,
                futureMaintenance
            });
        })
        .catch(e => res.status(400).json(e));
};

const createMaintenanceItem = (req, res) => {
    const maintenanceData = {
        ...req.body,
        carId: req.params.id
    };
    if (maintenanceData.targetDate) {
        const validDate = validateDateTime(maintenanceData.targetDate);
        if (!validDate) {
            return res.status(400).json({
                msg: 'Invalid date format for "targetDate"'
            });
        }
        maintenanceData.targetDate = getDateTimeString(validDate);
    }
    MaintenanceItemModel.create(maintenanceData)
        .then(maintenanceItem => res.status(200).json(maintenanceItem))
        .catch(e => res.status(400).json(e));
};

const getMaintenanceItem = (req, res) => {
    MaintenanceItemModel.findOne({
        where: { id: req.params.m_id, carId: req.params.id },
        include: 'car'
    })
        .then(maintenanceItem => {
            res.status(200).json(getMaintenanceItemPriority(maintenanceItem));
        })
        .catch(e => res.status(400).json(e));
};

const updateMaintenanceItem = (req, res) => {
    const maintenanceData = { ...req.body };
    if (maintenanceData.targetDate) {
        const validDate = validateDateTime(maintenanceData.targetDate);
        if (validDate) {
            maintenanceData.targetDate = getDateTimeString(validDate);
        } else {
            return res.status(400).json({
                msg: 'Invalid date format for "targetDate"'
            });
        }
    }
    MaintenanceItemModel.findOne({
        where: { id: req.params.m_id, carId: req.params.id }
    })
        .then(maintenanceItem => maintenanceItem.update(maintenanceData))
        .then(maintenanceItem => res.status(200).json(maintenanceItem))
        .catch(e => res.status(400).json(e));
};

const deleteMaintenanceItem = (req, res) => {
    MaintenanceItemModel.findOne({
        where: { id: req.params.m_id, carId: req.params.id }
    })
        .then(maintenanceItem => maintenanceItem.destroy())
        .then(something => res.status(200).json(something))
        .catch(e => res.status(400).json(e));
};

module.exports = {
    getCarMaintenanceItems,
    createMaintenanceItem,
    getMaintenanceItem,
    updateMaintenanceItem,
    deleteMaintenanceItem
};
