const userController = require('../controllers/userController');
const carController = require('../controllers/carController');
const maintenanceController = require('../controllers/maintenanceController');

const carOwnerShareOrAdmin = require('../middleware/carOwnerShareOrAdmin');

module.exports = app => {
    // route-specific middleware
    app.use('/api/v1/car/:id', carOwnerShareOrAdmin);

    app.use('/api/v1', userController);
    app.use('/api/v1', carController);
    app.use('/api/v1', maintenanceController);
};
