import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Modal, Button, Icon } from 'semantic-ui-react';

import dayjs from '../../misc/dayjs';
import { DATE_PARSE_FORMAT, DATE_FORMAT_STRING } from '../../misc/constants';
import {
    addSharedCarUserAsync,
    deleteSharedCarUserAsync
} from '../../actions/carActions';

class SharedUsersModal extends React.Component {
    static propTypes = {
        open: PropTypes.bool,
        toggleModal: PropTypes.func,
        sharedUsers: PropTypes.array,
        match: PropTypes.object,
        addSharedCarUser: PropTypes.func,
        deleteSharedCarUser: PropTypes.func
    };

    static defaultProps = {
        sharedUsers: []
    };

    constructor(props) {
        super(props);
        this.state = {
            newEmail: ''
        };
    }

    handleNewEmailChange = e => {
        e.persist();
        this.setState(() => ({ newEmail: e.target.value }));
    };

    handleEmailEnter = e => {
        if (e.key === 'Enter') {
            this.handleAddSharedUser();
        }
    };

    handleDeleteSharedUser = email => {
        const carId = this.props.match.params.id;
        this.props.deleteSharedCarUser(carId, email);
    };

    handleAddSharedUser = () => {
        if (this.state.newEmail.trim()) {
            const carId = this.props.match.params.id;
            this.props.addSharedCarUser(
                carId,
                this.state.newEmail.trim().toLowerCase()
            );
            this.setState(() => ({ newEmail: '' }));
        }
    };

    render() {
        return (
            <Modal
                open={this.props.open}
                onClose={this.props.toggleModal}
                basic
                className="shared-users"
                size="tiny"
            >
                <Modal.Header>
                    <span className="flexbox-space-between">
                        <span>Manage shared users</span>
                        <Button
                            onClick={this.props.toggleModal}
                            icon
                            color="red"
                            circular
                            inverted
                        >
                            <Icon name="times" />
                        </Button>
                    </span>
                </Modal.Header>
                <Modal.Content>
                    <div className="list-container">
                        {this.props.sharedUsers.map(user => (
                            <div className="item-container" key={user.email}>
                                <div className="item">
                                    <div className="delete">
                                        <Icon
                                            name="times"
                                            color="red"
                                            className="clickable"
                                            onClick={() =>
                                                this.handleDeleteSharedUser(
                                                    user.email
                                                )
                                            }
                                        />
                                    </div>
                                    <div className="user">
                                        <div>{user.email}</div>
                                        <div>
                                            added{' '}
                                            {dayjs
                                                .utc(
                                                    user.createdAt,
                                                    DATE_PARSE_FORMAT
                                                )
                                                .format(DATE_FORMAT_STRING)}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ))}
                        <div className="new-shared-user-container">
                            <div className="new-shared-user">
                                <div className="add">
                                    <Icon
                                        name="plus"
                                        color="green"
                                        className="clickable"
                                        onClick={this.handleAddSharedUser}
                                    />
                                </div>
                                <div className="input-container">
                                    <input
                                        type="text"
                                        placeholder="Email to share with"
                                        value={this.state.newEmail}
                                        onChange={this.handleNewEmailChange}
                                        onKeyPress={this.handleEmailEnter}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Content>
            </Modal>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    addSharedCarUser: (carId, email) =>
        dispatch(addSharedCarUserAsync(carId, email)),
    deleteSharedCarUser: (carId, email) =>
        dispatch(deleteSharedCarUserAsync(carId, email))
});

export default connect(
    undefined,
    mapDispatchToProps
)(withRouter(SharedUsersModal));
