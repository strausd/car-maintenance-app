import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Modal, Button, Icon, Form, Grid, Input } from 'semantic-ui-react';

import { addCarAsync } from '../../actions/carActions';

class NewCarModal extends React.Component {
    static propTypes = {
        open: PropTypes.bool,
        toggleOpen: PropTypes.func,
        addCar: PropTypes.func,
        newCarModalClass: PropTypes.string
    };

    constructor(props) {
        super(props);
        this.state = {
            make: '',
            model: '',
            year: '',
            color: '',
            nickname: '',
            miles: '',
            mileage: ''
        };
    }

    handleSubmit = e => {
        e.preventDefault();
        const {
            make,
            model,
            year,
            color,
            nickname,
            miles,
            mileage
        } = this.state;
        this.props.addCar({
            make,
            model,
            year,
            color,
            nickname,
            miles,
            mileage: mileage || null
        });
        this.props.toggleOpen();
    };

    handleMakeChange = e => {
        e.persist();
        this.setState(() => ({ make: e.target.value }));
    };

    handleModelChange = e => {
        e.persist();
        this.setState(() => ({ model: e.target.value }));
    };

    handleYearChange = e => {
        e.persist();
        this.setState(() => ({ year: e.target.value }));
    };

    handleColorChange = e => {
        e.persist();
        this.setState(() => ({ color: e.target.value }));
    };

    handleNicknameChange = e => {
        e.persist();
        this.setState(() => ({ nickname: e.target.value }));
    };

    handleMilesChange = e => {
        e.persist();
        this.setState(() => ({ miles: e.target.value }));
    };

    handleMileageChange = e => {
        e.persist();
        this.setState(() => ({ mileage: e.target.value }));
    };

    render() {
        return (
            <Modal
                basic
                open={this.props.open}
                onClose={this.props.toggleOpen}
                className={`add-maintenance new-car-modal ${this.props.newCarModalClass}`}
            >
                <Modal.Header>
                    <span className="flexbox-space-between">
                        <span>New Car</span>
                        <Button
                            onClick={this.props.toggleOpen}
                            icon
                            color="red"
                            circular
                            inverted
                        >
                            <Icon name="times" />
                        </Button>
                    </span>
                </Modal.Header>
                <Modal.Content>
                    <Form onSubmit={this.handleSubmit}>
                        <Grid container columns="equal" stackable>
                            <Grid.Row>
                                <Grid.Column>
                                    <span className="bold">Make*</span>
                                    <Input
                                        fluid
                                        placeholder="Make"
                                        value={this.state.make}
                                        onChange={this.handleMakeChange}
                                    />
                                </Grid.Column>
                                <Grid.Column>
                                    <span className="bold">Model*</span>
                                    <Input
                                        fluid
                                        placeholder="Model"
                                        value={this.state.model}
                                        onChange={this.handleModelChange}
                                    />
                                </Grid.Column>
                                <Grid.Column>
                                    <span className="bold">Year*</span>
                                    <Input
                                        fluid
                                        placeholder="Year"
                                        type="number"
                                        min="1885"
                                        max="2200"
                                        value={this.state.year}
                                        onChange={this.handleYearChange}
                                    />
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column>
                                    <span className="bold">Color*</span>
                                    <Input
                                        fluid
                                        placeholder="Color"
                                        value={this.state.color}
                                        onChange={this.handleColorChange}
                                    />
                                </Grid.Column>
                                <Grid.Column>
                                    <span className="bold">Nickname</span>
                                    <Input
                                        fluid
                                        placeholder="Nickname"
                                        value={this.state.nickname}
                                        onChange={this.handleNicknameChange}
                                    />
                                </Grid.Column>
                                <Grid.Column>
                                    <span className="bold">Miles*</span>
                                    <Input
                                        fluid
                                        placeholder="Miles"
                                        type="number"
                                        value={this.state.miles}
                                        onChange={this.handleMilesChange}
                                    />
                                </Grid.Column>
                                <Grid.Column>
                                    <span className="bold">Yearly Mileage</span>
                                    <Input
                                        fluid
                                        placeholder="Yearly Mileage"
                                        type="number"
                                        value={this.state.mileage}
                                        onChange={this.handleMileageChange}
                                    />
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Column width={16}>
                                <Button
                                    animated
                                    inverted
                                    color="green"
                                    className="submit-button round-button"
                                    onClick={this.handleSubmit}
                                >
                                    <Button.Content visible>Add</Button.Content>
                                    <Button.Content hidden>
                                        <Icon name="arrow right" />
                                    </Button.Content>
                                </Button>
                            </Grid.Column>
                        </Grid>
                    </Form>
                </Modal.Content>
            </Modal>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    addCar: carData => dispatch(addCarAsync(carData))
});

export default connect(undefined, mapDispatchToProps)(NewCarModal);
