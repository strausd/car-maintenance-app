import React from 'react';
import PropTypes from 'prop-types';
import { Button, Icon } from 'semantic-ui-react';

import AddMaintenanceModal from './AddMaintenanceModal';

class AddMaintenanceButton extends React.Component {
    static propTypes = {
        past: PropTypes.bool,
        carId: PropTypes.number
    };

    static defaultProps = {
        past: false
    };

    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false
        };
    }

    toggleModal = () =>
        this.setState(prevState => ({ isModalOpen: !prevState.isModalOpen }));

    render() {
        return (
            <>
                <div className="flexbox-center">
                    <Button
                        fluid
                        animated="fade"
                        className="basic-center round-button"
                        color="black"
                        onClick={this.toggleModal}
                    >
                        <Button.Content visible>
                            <Icon name="wrench" />
                            Add {this.props.past ? 'Past' : 'Future'}{' '}
                            Maintenance
                        </Button.Content>
                        <Button.Content hidden>
                            <Icon name="plus" />
                        </Button.Content>
                    </Button>
                </div>
                <AddMaintenanceModal
                    open={this.state.isModalOpen}
                    toggleOpen={this.toggleModal}
                    past={this.props.past}
                    carId={this.props.carId}
                />
            </>
        );
    }
}

export default AddMaintenanceButton;
