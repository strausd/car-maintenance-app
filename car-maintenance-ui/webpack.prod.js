const { merge } = require('webpack-merge');
const webpack = require('webpack');

const baseConfig = require('./webpack.config');

const environmentVariables = {
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    'process.env.GOOGLE_CLIENT_ID': JSON.stringify(process.env.GOOGLE_CLIENT_ID)
};

const prodConfig = {
    mode: 'production',
    devtool: 'cheap-module-source-map',
    plugins: [new webpack.DefinePlugin(environmentVariables)]
};

module.exports = merge(baseConfig, prodConfig);
