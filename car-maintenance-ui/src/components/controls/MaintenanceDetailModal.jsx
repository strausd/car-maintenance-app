import React from 'react';
import PropTypes from 'prop-types';
import {
    Modal,
    Button,
    Table,
    Icon,
    Form,
    Grid,
    Input
} from 'semantic-ui-react';
import { connect } from 'react-redux';

import dayjs from '../../misc/dayjs';
import {
    updateMaintenanceItemAsync,
    deleteMaintenanceItemAsync
} from '../../actions/carActions';
import { addCommasToNumbers } from '../../misc/util';
import {
    DATE_FORMAT_STRING,
    DATE_PARSE_FORMAT,
    DATE_SIMPLE_STRING
} from '../../misc/constants';

class MaintenanceDetailModal extends React.Component {
    static propTypes = {
        open: PropTypes.bool,
        onClose: PropTypes.func,
        item: PropTypes.object,
        updateMaintenanceItem: PropTypes.func,
        deleteMaintenanceItem: PropTypes.func
    };

    constructor(props) {
        super(props);
        this.defaultDate = dayjs().format(DATE_SIMPLE_STRING);
        this.defaultMiles = '';
        this.state = {
            showCompleteFields: false,
            completedDate: this.defaultDate,
            completedMiles: this.defaultMiles
        };
    }

    setCompletedDate = e => {
        e.persist();
        this.setState(() => ({ completedDate: e.target.value }));
    };

    setCompletedMiles = e => {
        e.persist();
        this.setState(() => ({ completedMiles: e.target.value }));
    };

    handleCompleteMaintenanceItem = () => {
        if (this.state.showCompleteFields) {
            this.props.updateMaintenanceItem(
                this.props.item.carId,
                this.props.item.id,
                {
                    isComplete: true,
                    completedDate: this.state.completedDate || null,
                    completedMiles: this.state.completedMiles || null
                }
            );
            this.handleClose();
        } else {
            this.setState(() => ({ showCompleteFields: true }));
        }
    };

    handleDeleteMaintenanceItem = () => {
        this.props.deleteMaintenanceItem(
            this.props.item.carId,
            this.props.item.id
        );
        this.handleClose();
    };

    handleClose = () => {
        this.setState(() => ({
            showCompleteFields: false,
            completedDate: this.defaultDate,
            completedMiles: this.defaultMiles
        }));
        this.props.onClose();
    };

    render() {
        const targetDate = dayjs.utc(
            this.props.item.targetDate,
            DATE_PARSE_FORMAT
        );
        return (
            <Modal
                open={this.props.open}
                onClose={this.handleClose}
                basic
                size="small"
                className="maintenance-detail-modal"
            >
                <Modal.Header>
                    <span className="flexbox-space-between">
                        <span>{this.props.item.title}</span>
                        <Button
                            onClick={this.handleClose}
                            icon
                            color="red"
                            circular
                            inverted
                        >
                            <Icon name="times" />
                        </Button>
                    </span>
                </Modal.Header>
                <Modal.Content>
                    <Table basic="very" celled>
                        <Table.Body>
                            <Table.Row>
                                <Table.Cell width={4}>Target Date</Table.Cell>
                                <Table.Cell>
                                    {this.props.item.targetDate
                                        ? targetDate.format(DATE_FORMAT_STRING)
                                        : 'N/A'}
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>Target Miles</Table.Cell>
                                <Table.Cell>
                                    {addCommasToNumbers(
                                        this.props.item.targetMiles
                                    ) || 'N/A'}
                                </Table.Cell>
                            </Table.Row>
                            {this.renderCompleteAttributes()}
                            <Table.Row>
                                <Table.Cell>Status</Table.Cell>
                                <Table.Cell>
                                    {this.props.item.isComplete === true
                                        ? 'Complete'
                                        : 'Incomplete'}
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>Description</Table.Cell>
                                <Table.Cell>
                                    {this.props.item.description || 'N/A'}
                                </Table.Cell>
                            </Table.Row>
                        </Table.Body>
                    </Table>
                    {this.renderCompleteFields()}
                    {this.renderComplete()}
                    <Button
                        fluid
                        inverted
                        color="red"
                        className="round-button"
                        onClick={this.handleDeleteMaintenanceItem}
                    >
                        Delete
                    </Button>
                </Modal.Content>
            </Modal>
        );
    }

    renderComplete = () => {
        if (!this.props.item.isComplete) {
            const buttonText = this.state.showCompleteFields
                ? 'Save'
                : 'Complete this item';
            return (
                <Button
                    fluid
                    inverted
                    color="green"
                    className="round-button"
                    onClick={() => this.handleCompleteMaintenanceItem()}
                >
                    {buttonText}
                </Button>
            );
        }
        return undefined;
    };

    renderCompleteAttributes = () => {
        if (this.props.item.isComplete) {
            const completedDate = dayjs.utc(
                this.props.item.completedDate,
                DATE_PARSE_FORMAT
            );
            return (
                <>
                    <Table.Row>
                        <Table.Cell width={4}>Completed Date</Table.Cell>
                        <Table.Cell>
                            {this.props.item.completedDate
                                ? completedDate.format(DATE_FORMAT_STRING)
                                : 'N/A'}
                        </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>Completed Miles</Table.Cell>
                        <Table.Cell>
                            {addCommasToNumbers(
                                this.props.item.completedMiles
                            ) || 'N/A'}
                        </Table.Cell>
                    </Table.Row>
                </>
            );
        }
        return undefined;
    };

    renderCompleteFields = () => {
        if (this.state.showCompleteFields) {
            return (
                <Form className="complete-item-form">
                    <div>
                        <span className="bold">Completed Date</span>
                        <Input
                            type="date"
                            fluid
                            placeholder="What date was this done?"
                            value={this.state.completedDate}
                            onChange={this.setCompletedDate}
                        />
                    </div>
                    <div>
                        <span className="bold">Completed Miles</span>
                        <Input
                            type="number"
                            fluid
                            placeholder="At how many miles was this done?"
                            value={this.state.completedMiles}
                            onChange={this.setCompletedMiles}
                        />
                    </div>
                </Form>
            );
        }
        return undefined;
    };
}

const mapDispatchToProps = dispatch => ({
    updateMaintenanceItem: (carId, maintenanceId, mi) =>
        dispatch(updateMaintenanceItemAsync(carId, maintenanceId, mi)),
    deleteMaintenanceItem: (carId, maintenanceId) =>
        dispatch(deleteMaintenanceItemAsync(carId, maintenanceId))
});

export default connect(undefined, mapDispatchToProps)(MaintenanceDetailModal);
