const ADMIN_ONLY_URLS = {
    get: [/^\/api\/v1\/user$/],
    patch: [/^\/api\/v1\/user\/[0-9]+\/isAdmin$/]
};

module.exports = (req, res, next) => {
    const method = req.method.toLowerCase();
    if (
        ADMIN_ONLY_URLS[method] &&
        ADMIN_ONLY_URLS[method].some(pattern => pattern.test(req.url))
    ) {
        if (res.locals.user.isAdmin) {
            next();
        } else {
            res.status(403).json({ msg: 'Unauthorized' });
        }
    } else {
        next();
    }
};
