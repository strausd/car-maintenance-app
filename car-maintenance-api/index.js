const path = require('path');
const express = require('express');

if (process.env.NODE_ENV === 'development') {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
    require('dotenv').config();
}

const apiRoutes = require('./routes/routes');
const authMiddleware = require('./middleware/auth');
const adminOnly = require('./middleware/adminOnly');

if (process.env.NODE_ENV === 'development') {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
}

const app = express();
const port = process.env.PORT || 8001;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(authMiddleware);
app.use(adminOnly);
apiRoutes(app);

const publicPath = path.join(__dirname, 'public');
app.use(express.static(publicPath));
app.get('*', (req, res) => {
    res.sendFile(path.join(publicPath, 'index.html'));
});

app.listen(port, () => console.log(`App started on port ${port}!`));
