const express = require('express');

const userService = require('../service/userService');

const router = express.Router();

router.get('/user', (req, res) => {
    userService.getAllUsers(req, res);
});
router.post('/user', (req, res) => {
    userService.createUser(req, res);
});
router.patch('/user/:id/isAdmin', (req, res) => {
    userService.updateUser(req, res);
});

module.exports = router;
