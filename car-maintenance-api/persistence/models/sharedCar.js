const { DataTypes } = require('sequelize');

const SharedCarModel = sequelize => {
    return sequelize.define('shared_cars', {
        carId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'car_id',
            references: {
                model: 'cars',
                field: 'id'
            },
            primaryKey: true
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: 'email',
            primaryKey: true
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: false,
            field: 'updated_at'
        }
    });
};

module.exports = SharedCarModel;
