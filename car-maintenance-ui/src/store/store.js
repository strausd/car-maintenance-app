import { createStore, combineReducers, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import authReducer from '../reducers/authReducer';
import carsReducer from '../reducers/carsReducer';
import isLoadingCarsReducer from '../reducers/isLoadingCarsReducer';

const store = createStore(
    combineReducers({
        auth: authReducer,
        cars: carsReducer,
        isLoadingCars: isLoadingCarsReducer
    }),
    composeWithDevTools(applyMiddleware(thunk))
);

export default store;
