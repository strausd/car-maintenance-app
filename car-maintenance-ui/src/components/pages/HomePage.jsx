import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Transition } from 'react-transition-group';

import CarList from '../controls/CarsList';
import HomeLogin from '../controls/HomeLogin';

const duration = 500;
const defaultStyle = {
    transition: `opacity ${duration}ms ease-in-out, transform ${duration}ms ease-in-out`,
    opacity: 0,
    transform: 'translateY(10%)'
};
const transitionStyles = {
    entering: { opacity: 1, transform: 'translateY(0)' },
    entered: { opacity: 1, transform: 'translateY(0)' },
    exiting: { opacity: 0, transform: 'translateY(10%)' },
    exited: { opacity: 0, transform: 'translateY(10%)' }
};

const HomePage = ({ isAuthenticated }) => {
    return (
        <Transition in={isAuthenticated} timeout={duration}>
            {state => (
                <div
                    className="page home"
                    style={{ ...defaultStyle, ...transitionStyles[state] }}
                >
                    <CarList />
                </div>
            )}
        </Transition>
    );
};

HomePage.propTypes = {
    isAuthenticated: PropTypes.bool
};

HomePage.defaultProps = {
    isAuthenticated: false
};

const mapStateToProps = state => ({
    isAuthenticated: !!state.auth.accessToken && state.auth.userExists
});

export default connect(mapStateToProps)(HomePage);
