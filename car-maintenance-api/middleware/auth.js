const oAuth2Client = require('../clients/oAuth2');
const { UserModel } = require('../persistence/sequelize');

module.exports = (req, res, next) => {
    if (req.url.startsWith('/api')) {
        oAuth2Client
            .getTokenInfo(req.headers.access_token)
            .then(response => {
                if (response.code && response.code !== 200) {
                    return res.status(401).json({
                        message:
                            'Invalid access token. Please try logging in again.'
                    });
                }
                res.locals.user = {
                    email: response.email
                };
                return UserModel.findOne({
                    where: {
                        email: response.email
                    }
                });
            })
            .then(user => {
                if (user && user.id) {
                    res.locals.user = user;
                }
                next();
            })
            .catch(e => {
                const status = e.code ? e.code : 500;
                res.status(status).json({
                    message:
                        'Unknown error occurred while trying to validate the accessToken. Please try again later.',
                    e
                });
            });
    } else {
        next();
    }
};
