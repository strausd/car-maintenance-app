const Sequelize = require('sequelize');

const UserModelDefinition = require('./models/user');
const CarModelDefinition = require('./models/car');
const MaintenanceItemModelDefinition = require('./models/maintenanceItem');
const SharedCarModelDefinition = require('./models/sharedCar');

const log = require('../util/log');

const { DATABASE_URL } = process.env;
const dialectOptions = {};
const sslDialectOptions = {
    require: true,
    rejectUnauthorized: false
};
if (!DATABASE_URL.includes('localhost')) {
    dialectOptions.ssl = sslDialectOptions;
}

// TODO: Check that dates are being read by sequelize in the proper timezone
const sequelizeInstance = new Sequelize(DATABASE_URL, {
    dialect: 'postgres',
    dialectOptions,
    pool: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    timezone: '+00:00',
    logging: msg => log.debug(msg)
});

const UserModel = UserModelDefinition(sequelizeInstance);
const CarModel = CarModelDefinition(sequelizeInstance);
const MaintenanceItemModel = MaintenanceItemModelDefinition(sequelizeInstance);
const SharedCarModel = SharedCarModelDefinition(sequelizeInstance);

// set user and car relationship
UserModel.hasMany(CarModel, {
    as: 'cars',
    foreignKey: 'ownerId',
    onDelete: 'CASCADE',
    hooks: true
});
CarModel.belongsTo(UserModel, {
    as: 'owner',
    foreignKey: 'ownerId'
});

// set car and maintenance relationship
CarModel.hasMany(MaintenanceItemModel, {
    as: 'maintenance_items',
    foreignKey: 'carId',
    onDelete: 'CASCADE',
    hooks: true
});
MaintenanceItemModel.belongsTo(CarModel, {
    as: 'car',
    foreignKey: 'carId'
});

// set car and shared car relationship
CarModel.hasMany(SharedCarModel, {
    as: 'sharedCars',
    foreignKey: 'carId',
    onDelete: 'CASCADE',
    hooks: true
});
SharedCarModel.belongsTo(CarModel, {
    as: 'car',
    foreignKey: 'carId'
});

log.info('Waiting on database sync...');
sequelizeInstance.sync().then(() => log.info('DB Sync Complete'));

module.exports = {
    UserModel,
    CarModel,
    MaintenanceItemModel,
    SharedCarModel
};
