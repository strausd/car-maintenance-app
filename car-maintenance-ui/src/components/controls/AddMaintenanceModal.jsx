import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    Modal,
    Input,
    Grid,
    Form,
    TextArea,
    Dropdown,
    Button,
    Icon
} from 'semantic-ui-react';

import dayjs from '../../misc/dayjs';
import { addMaintenenaceItemAsync } from '../../actions/carActions';
import { DATE_SIMPLE_STRING } from '../../misc/constants';

class AddMaintenanceModal extends React.Component {
    static propTypes = {
        open: PropTypes.bool,
        toggleOpen: PropTypes.func,
        addMaintenance: PropTypes.func,
        carOptions: PropTypes.array,
        carId: PropTypes.number,
        past: PropTypes.bool
    };

    static defaultProps = {
        carOptions: [],
        past: false
    };

    constructor(props) {
        super(props);
        this.state = {
            title: '',
            description: '',
            targetDate: '',
            targetMiles: '',
            completedDate: props.past ? dayjs().format(DATE_SIMPLE_STRING) : '',
            completedMiles: '',
            selectedCar: props.carId || ''
        };
    }

    setTitle = e => {
        e.persist();
        this.setState(() => ({ title: e.target.value }));
    };

    setDescription = e => {
        e.persist();
        this.setState(() => ({ description: e.target.value }));
    };

    setTargetDate = e => {
        e.persist();
        this.setState(() => ({ targetDate: e.target.value }));
    };

    setTargetMiles = e => {
        e.persist();
        this.setState(() => ({ targetMiles: e.target.value }));
    };

    setCompletedDate = e => {
        e.persist();
        console.log(e.target.value);
        this.setState(() => ({ completedDate: e.target.value }));
    };

    setCompletedMiles = e => {
        e.persist();
        this.setState(() => ({ completedMiles: e.target.value }));
    };

    setCar = (e, data) => {
        this.setState(() => ({ selectedCar: data.value }));
    };

    clearInputValues = () => {
        this.setState(() => ({
            title: '',
            description: '',
            targetDate: '',
            targetMiles: '',
            completedDate: this.props.past
                ? dayjs().format(DATE_SIMPLE_STRING)
                : '',
            completedMiles: ''
        }));
    };

    handleSubmit = e => {
        e.preventDefault();
        const targetDate = dayjs.utc(this.state.targetDate, DATE_SIMPLE_STRING);
        const completedDate = dayjs.utc(
            this.state.completedDate,
            DATE_SIMPLE_STRING
        );
        this.props.addMaintenance(this.state.selectedCar, {
            title: this.state.title.trim(),
            description: this.state.description || null,
            targetDate: this.state.targetDate ? targetDate : null,
            targetMiles: this.state.targetMiles || null,
            completedDate: this.state.completedDate ? completedDate : null,
            completedMiles: this.state.completedMiles || null,
            isComplete: this.props.past
        });
        this.clearInputValues();
        this.props.toggleOpen();
    };

    render() {
        return (
            <Modal
                open={this.props.open}
                onClose={this.props.toggleOpen}
                basic
                className="add-maintenance"
            >
                <Modal.Header>
                    <span className="flexbox-space-between">
                        <span>New Maintenance Item</span>
                        <Button
                            onClick={this.props.toggleOpen}
                            icon
                            color="red"
                            circular
                            inverted
                        >
                            <Icon name="times" />
                        </Button>
                    </span>
                </Modal.Header>
                <Modal.Content>
                    <Form>
                        <Grid container columns="equal">
                            <Grid.Column width={16}>
                                <span className="bold">Title</span>
                                <Input
                                    fluid
                                    placeholder="What needs to be done?"
                                    value={this.state.title}
                                    onChange={this.setTitle}
                                />
                            </Grid.Column>
                            <Grid.Column width={16}>
                                <span className="bold">Description</span>
                                <TextArea
                                    placeholder="What are the details of this maintenance item?"
                                    value={this.state.description}
                                    onChange={this.setDescription}
                                />
                            </Grid.Column>
                            <Grid.Column width={16}>
                                <span className="bold">Target Date</span>
                                <Input
                                    type="date"
                                    className="date-input"
                                    fluid
                                    placeholder="What date should this be done?"
                                    value={this.state.targetDate}
                                    onChange={this.setTargetDate}
                                    icon={{
                                        name: 'times',
                                        circular: true,
                                        link: true,
                                        color: 'red',
                                        onClick: () =>
                                            this.setState(() => ({
                                                targetDate: ''
                                            }))
                                    }}
                                />
                            </Grid.Column>
                            <Grid.Column width={16}>
                                <span className="bold">Target Miles</span>
                                <Input
                                    type="number"
                                    fluid
                                    placeholder="At how many miles should this be done?"
                                    value={this.state.targetMiles}
                                    onChange={this.setTargetMiles}
                                />
                            </Grid.Column>
                            {this.renderCompletedFields()}
                            <Grid.Column width={16}>
                                <span className="bold">Car</span>
                                <Dropdown
                                    fluid
                                    placeholder="What car is this maintenance item for?"
                                    selection
                                    value={this.state.selectedCar}
                                    onChange={this.setCar}
                                    options={this.props.carOptions}
                                />
                            </Grid.Column>
                            <Grid.Column width={16}>
                                <Button
                                    animated
                                    inverted
                                    color="green"
                                    className="submit-button round-button"
                                    onClick={this.handleSubmit}
                                >
                                    <Button.Content visible>
                                        Submit
                                    </Button.Content>
                                    <Button.Content hidden>
                                        <Icon name="arrow right" />
                                    </Button.Content>
                                </Button>
                            </Grid.Column>
                        </Grid>
                    </Form>
                </Modal.Content>
            </Modal>
        );
    }

    renderCompletedFields = () => {
        if (this.props.past) {
            return (
                <>
                    <Grid.Column width={16}>
                        <span className="bold">Completed Date</span>
                        <Input
                            type="date"
                            className="date-input"
                            fluid
                            placeholder="What date was this done?"
                            value={this.state.completedDate}
                            onChange={this.setCompletedDate}
                            icon={{
                                name: 'times',
                                circular: true,
                                link: true,
                                color: 'red',
                                onClick: () =>
                                    this.setState(() => ({
                                        completedDate: ''
                                    }))
                            }}
                        />
                    </Grid.Column>
                    <Grid.Column width={16}>
                        <span className="bold">Completed Miles</span>
                        <Input
                            type="number"
                            fluid
                            placeholder="At how many miles was this done?"
                            value={this.state.completedMiles}
                            onChange={this.setCompletedMiles}
                        />
                    </Grid.Column>
                </>
            );
        }
        return undefined;
    };
}

const mapStateToProps = ({ cars }) => ({
    carOptions: cars.map(car => ({
        key: car.id,
        text: `${car.year} ${car.make} ${car.model}`,
        value: car.id
    }))
});

const mapDispatchToProps = dispatch => ({
    addMaintenance: (carId, maintenanceData) =>
        dispatch(addMaintenenaceItemAsync(carId, maintenanceData))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddMaintenanceModal);
