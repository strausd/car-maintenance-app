const { UserModel } = require('../persistence/sequelize');

const getAllUsers = (req, res) => {
    UserModel.findAll().then(users => {
        res.status(200).json({ users });
    });
};

const createUser = (req, res) => {
    UserModel.findOrCreate({
        where: {
            email: res.locals.user.email
        },
        defaults: {
            isAdmin: false
        }
    })
        .then(users => {
            res.status(200).json({ users });
        })
        .catch(e => res.status(500).json({ message: 'Error', e }));
};

const updateUser = (req, res) => {
    UserModel.findOne({
        where: {
            id: req.params.id
        }
    })
        .then(user => {
            return user.update({ isAdmin: req.body.isAdmin });
        })
        .then(user => {
            res.status(200).json({
                msg: `Successfully set isAdmin for ${user.email} to ${user.isAdmin}`
            });
        })
        .catch(e => console.log(e));
};

module.exports = {
    getAllUsers,
    createUser,
    updateUser
};
