const defaultState = { profile: {}, userExists: false };

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'SET_AUTH':
            return action.auth;
        case 'CLEAR_AUTH':
            return defaultState;
        case 'SET_USER_EXISTS':
            return {
                ...state,
                userExists: !!action.userId,
                userId: action.userId
            };
        case 'SET_USER_ADMIN':
            return {
                ...state,
                profile: {
                    ...state.profile,
                    isAdmin: action.isAdmin
                }
            };
        default:
            return state;
    }
};
