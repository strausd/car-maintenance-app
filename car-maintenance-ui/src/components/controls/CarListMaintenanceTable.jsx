import React from 'react';
import PropTypes from 'prop-types';
import { Table, Grid } from 'semantic-ui-react';

import CarListMaintenanceTableItem from './CarListMaintenanceTableItem';
import AddMaintenanceButton from './AddMaintenanceButton';

const CarListMaintenanceTable = ({ maintenanceItems, past, carId }) => {
    const renderButton = () => (
        <Grid centered>
            <Grid.Column mobile={16} tablet={8} computer={6}>
                <AddMaintenanceButton past={past} carId={carId} />
            </Grid.Column>
        </Grid>
    );
    if (maintenanceItems.length === 0) {
        return renderButton();
    }

    const renderFutureHeaders = () => (
        <Table.Header>
            <Table.Row>
                <Table.HeaderCell>Title</Table.HeaderCell>
                <Table.HeaderCell>Target Date</Table.HeaderCell>
                <Table.HeaderCell>Target Miles</Table.HeaderCell>
            </Table.Row>
        </Table.Header>
    );

    const renderCompleteHeaders = () => (
        <Table.Header>
            <Table.Row>
                <Table.HeaderCell>Title</Table.HeaderCell>
                <Table.HeaderCell>Completed Date</Table.HeaderCell>
                <Table.HeaderCell>Completed Miles</Table.HeaderCell>
            </Table.Row>
        </Table.Header>
    );

    return (
        <>
            <div className="maintenance-list full-width">
                <Table
                    basic="very"
                    className="maintenance-table"
                    selectable
                    unstackable
                >
                    {past ? renderCompleteHeaders() : renderFutureHeaders()}
                    <Table.Body>
                        {maintenanceItems.map(item => (
                            <CarListMaintenanceTableItem
                                key={item.id}
                                item={item}
                            />
                        ))}
                    </Table.Body>
                </Table>
            </div>
            {renderButton()}
        </>
    );
};

CarListMaintenanceTable.propTypes = {
    maintenanceItems: PropTypes.array,
    past: PropTypes.bool,
    carId: PropTypes.number
};

CarListMaintenanceTable.defaultProps = {
    maintenanceItems: [],
    past: false
};

export default CarListMaintenanceTable;
