import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import HomePage from '../components/pages/HomePage';
import CarDetailPage from '../components/pages/CarDetailPage';
import Header from '../components/controls/Header';
import RequireAuth from '../components/controls/RequireAuth';

import routes from './routes';

const AppRouter = () => {
    return (
        <Router>
            <Header />
            <div>
                <Route path={routes.root} exact component={HomePage} />
                <RequireAuth
                    render={() => (
                        <Route
                            path={routes.carDetail}
                            component={CarDetailPage}
                        />
                    )}
                />
            </div>
        </Router>
    );
};

export default AppRouter;
