import store from '../store/store';

class Api {
    baseUrl = '/api/v1';

    getHeaders = () => {
        const { auth } = store.getState();
        return {
            access_token: auth.accessToken,
            'Content-Type': 'application/json'
        };
    };

    getFetchData = (url, options) => {
        // translate body object to JSON for fetch to work properly
        if (typeof options.body === 'object') {
            options.body = JSON.stringify(options.body);
        }
        return fetch(`${this.baseUrl}${url}`, {
            ...options,
            headers: this.getHeaders()
        }).then(response => {
            console.log(url, response.status);
            return response.json();
        });
    };

    getOrCreateUser = () => {
        return this.getFetchData('/user', {
            method: 'POST'
        });
    };

    getAllCarsForUser = () => {
        return this.getFetchData('/car', {
            method: 'GET'
        });
    };

    createCar = carData => {
        return this.getFetchData('/car', {
            method: 'POST',
            body: carData
        });
    };

    getCarDetails = id => {
        return this.getFetchData(`/car/${id}`, {
            method: 'GET'
        });
    };

    updateCar = (id, carDetails) => {
        return this.getFetchData(`/car/${id}`, {
            method: 'PATCH',
            body: carDetails
        });
    };

    deleteCar = id => {
        return this.getFetchData(`/car/${id}`, {
            method: 'DELETE'
        });
    };

    addSharedCarUser = (id, email) => {
        return this.getFetchData(`/car/${id}/share`, {
            method: 'POST',
            body: { email }
        });
    };

    deleteSharedCarUser = (id, email) => {
        return this.getFetchData(`/car/${id}/share`, {
            method: 'DELETE',
            body: { email }
        });
    };

    getCarMaintenanceItems = id => {
        return this.getFetchData(`/car/${id}/maintenance`, {
            method: 'GET'
        });
    };

    createMaintenanceItem = (carId, maintenanceItem) => {
        return this.getFetchData(`/car/${carId}/maintenance`, {
            method: 'POST',
            body: maintenanceItem
        });
    };

    updateMaintenanceItem = (carId, maintenanceId, maintenanceItemUpdates) => {
        return this.getFetchData(`/car/${carId}/maintenance/${maintenanceId}`, {
            method: 'PATCH',
            body: maintenanceItemUpdates
        });
    };

    deleteMaintenanceItem = (carId, maintenanceId) => {
        return this.getFetchData(`/car/${carId}/maintenance/${maintenanceId}`, {
            method: 'DELETE'
        });
    };
}

export default new Api();
