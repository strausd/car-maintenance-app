const fs = require('fs-extra');

const apiPublicDir = './public';
const uiPublicDir = '../car-maintenance-ui/public';

if (fs.existsSync(apiPublicDir)) {
    fs.removeSync(apiPublicDir);
}

fs.copySync(uiPublicDir, apiPublicDir);

console.log('UI code copied to API');
