import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Modal, Input, Grid, Form, Button, Icon } from 'semantic-ui-react';

import { editCarAsync } from '../../actions/carActions';

class EditCarModal extends React.Component {
    static propTypes = {
        open: PropTypes.bool,
        toggleModal: PropTypes.func,
        car: PropTypes.object,
        editCar: PropTypes.func
    };

    static defaultProps = {
        open: false,
        car: {}
    };

    constructor(props) {
        super(props);
        this.state = {
            miles: props.car.miles || '',
            mileage: props.car.mileage || ''
        };
    }

    componentDidUpdate(prevProps) {
        if (this.props.car.miles !== prevProps.car.miles) {
            this.setState(() => ({ miles: this.props.car.miles || '' }));
        }
        if (this.props.car.mileage !== prevProps.car.mileage) {
            this.setState(() => ({ mileage: this.props.car.mileage || '' }));
        }
    }

    setMiles = e => {
        e.persist();
        this.setState(() => ({ miles: e.target.value }));
    };

    setMileage = e => {
        e.persist();
        this.setState(() => ({ mileage: e.target.value }));
    };

    handleSubmit = e => {
        e.preventDefault();
        if (
            this.props.car.miles !== this.state.miles ||
            this.props.car.mileage !== this.state.mileage
        ) {
            this.props.editCar(this.props.car.id, {
                miles: this.state.miles,
                mileage: this.state.mileage
            });
        }
        this.props.toggleModal();
    };

    render() {
        return (
            <Modal
                open={this.props.open}
                onClose={this.props.toggleModal}
                basic
                className="edit-car"
            >
                <Modal.Header>
                    <span className="flexbox-space-between">
                        <span>Edit Car</span>
                        <Button
                            onClick={this.props.toggleModal}
                            icon
                            color="red"
                            circular
                            inverted
                        >
                            <Icon name="times" />
                        </Button>
                    </span>
                </Modal.Header>
                <Modal.Content>
                    <Form>
                        <Grid container columns="equal">
                            <Grid.Column width={16}>
                                <span className="bold">Miles</span>
                                <Input
                                    fluid
                                    placeholder="How many total miles has this car been driven?"
                                    value={this.state.miles}
                                    onChange={this.setMiles}
                                />
                            </Grid.Column>
                            <Grid.Column width={16}>
                                <span className="bold">Yearly Mileage</span>
                                <Input
                                    fluid
                                    placeholder="How many miles is this car driven each year?"
                                    value={this.state.mileage}
                                    onChange={this.setMileage}
                                />
                            </Grid.Column>
                            <Grid.Column width={16}>
                                <Button
                                    animated
                                    inverted
                                    color="green"
                                    className="submit-button round-button"
                                    onClick={this.handleSubmit}
                                >
                                    <Button.Content visible>
                                        Submit
                                    </Button.Content>
                                    <Button.Content hidden>
                                        <Icon name="arrow right" />
                                    </Button.Content>
                                </Button>
                            </Grid.Column>
                        </Grid>
                    </Form>
                </Modal.Content>
            </Modal>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    editCar: (carId, carDetails) => dispatch(editCarAsync(carId, carDetails))
});

export default connect(undefined, mapDispatchToProps)(EditCarModal);
