import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { Button, Popup, Icon } from 'semantic-ui-react';
import { GoogleLogin, GoogleLogout } from 'react-google-login';

import api from '../../apiControllers/api';
import routes from '../../routers/routes';
import { GOOGLE_CLIENT_ID } from '../../misc/constants';
import {
    setAuth,
    clearAuth,
    setUserExists,
    setUserAdmin
} from '../../actions/authActions';
import { clearCars } from '../../actions/carActions';

class Header extends React.Component {
    static propTypes = {
        isAuthenticated: PropTypes.bool,
        firstName: PropTypes.string,
        setAuth: PropTypes.func,
        clearAuth: PropTypes.func,
        clearCars: PropTypes.func,
        setUserExists: PropTypes.func,
        setUserAdmin: PropTypes.func,
        history: PropTypes.object
    };

    static defaultProps = {
        isAuthenticated: false
    };

    constructor(props) {
        super(props);
        this.state = {
            renderExtraLogin: true,
            isPopupOpen: false,
            extraCssClass: '',
            shouldFadeOut: false
        };
    }

    handleLoginSuccess = response => {
        this.setState(() => ({
            renderExtraLogin: false
        }));
        this.props.setAuth({
            accessToken: response.accessToken,
            profile: response.profileObj
        });
        api.getOrCreateUser()
            .then(({ users }) => {
                this.props.setUserAdmin(users[0].isAdmin);
                this.props.setUserExists(users[0].id);
            })
            .catch(e => console.error(e));
        setTimeout(() => {
            window.location.reload();
        }, (response.tokenObj.expires_in - 60) * 1000);
    };

    handleLoginFailure = e => {
        console.error(e);
        // TODO: add pretty error logging
        console.error('Sign in failure');
        this.props.clearAuth();
    };

    handleLogoutSuccess = () => {
        this.props.clearAuth();
        this.props.clearCars();
        this.props.history.push(routes.root);
    };

    handleLogoutFailure = () => {
        // TODO: add pretty failures
        console.error('Sign out failure');
    };

    onPopupOpen = () => {
        this.setState(() => ({ isPopupOpen: true, shouldFadeOut: false }));
        // adding fade class needs to happen after popup gets added to dom so the css transition does its thing
        setTimeout(() => {
            this.setState(() => ({ extraCssClass: 'fade' }));
        }, 1);
    };

    onPopupClose = () => {
        this.setState(() => ({
            extraCssClass: '',
            shouldFadeOut: true
        }));
        setTimeout(() => {
            if (this.state.shouldFadeOut) {
                this.setState(() => ({
                    isPopupOpen: false
                }));
            }
        }, 250);
    };

    handleClickAction = onClick => {
        this.onPopupClose();
        onClick();
    };

    render() {
        const MyAccountContent = this.renderMyAccountContent;
        return (
            <div className="page-header">
                <div className="flex-container">
                    <Link to={routes.root} className="title">
                        Car Maintenance
                    </Link>
                </div>
                <div className="flex-container">
                    <span className="my-account">
                        <Popup
                            basic
                            inverted
                            className={`account-popup-container ${this.state.extraCssClass}`}
                            content={<MyAccountContent />}
                            hoverable
                            onOpen={this.onPopupOpen}
                            onClose={this.onPopupClose}
                            open={this.state.isPopupOpen}
                            trigger={this.renderPopupTrigger()}
                        />
                    </span>
                </div>
                {this.renderExtraLogin()}
            </div>
        );
    }

    renderExtraLogin = () => {
        if (this.state.renderExtraLogin) {
            return <div className="cm-hidden">{this.renderLoginButton()}</div>;
        }
        return null;
    };

    renderPopupTrigger = () => (
        <Button
            icon="user"
            inverted
            color="blue"
            size="large"
            className="round-button"
        />
    );

    renderMyAccountContent = () => {
        if (this.props.isAuthenticated) {
            return (
                <div className="account-popup">
                    <div className="popup-row">
                        Welcome, {this.props.firstName}
                    </div>
                    {/* <div className="popup-row">
                        <Button
                            icon
                            labelPosition="left"
                            inverted
                            color="blue"
                            className="popup-row round-button"
                            onClick={() => console.log('Redirect')}
                        >
                            <Icon name="setting" /> Account
                        </Button>
                    </div> */}
                    {this.renderLogoutButton()}
                </div>
            );
        }
        return <div className="account-popup">{this.renderLoginButton()}</div>;
    };

    renderLoginButton = () => {
        return (
            <GoogleLogin
                clientId={GOOGLE_CLIENT_ID}
                onSuccess={this.handleLoginSuccess}
                onFailure={this.handleLoginFailure}
                onRequest={() => {}}
                cookiePolicy="single_host_origin"
                isSignedIn
                autoLoad={false}
                render={({ onClick }) => (
                    <Button
                        icon
                        labelPosition="left"
                        inverted
                        color="blue"
                        className="popup-row round-button"
                        onClick={() => this.handleClickAction(onClick)}
                    >
                        <Icon name="google" /> Login
                    </Button>
                )}
            />
        );
    };

    renderLogoutButton = () => {
        return (
            <GoogleLogout
                clientId={GOOGLE_CLIENT_ID}
                onLogoutSuccess={this.handleLogoutSuccess}
                onFailure={this.handleLogoutFailure}
                render={({ onClick }) => (
                    <Button
                        icon
                        labelPosition="left"
                        inverted
                        color="blue"
                        className="popup-row round-button"
                        onClick={() => this.handleClickAction(onClick)}
                    >
                        <Icon name="google" /> Logout
                    </Button>
                )}
            />
        );
    };
}

const mapStateToProps = ({ auth }) => ({
    isAuthenticated: !!auth.accessToken,
    firstName: auth.profile.givenName
});

const mapDispatchToProps = dispatch => ({
    setAuth: auth => dispatch(setAuth(auth)),
    clearAuth: () => dispatch(clearAuth()),
    setUserExists: userExists => dispatch(setUserExists(userExists)),
    setUserAdmin: isAdmin => dispatch(setUserAdmin(isAdmin)),
    clearCars: () => dispatch(clearCars())
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Header));
