const defaultState = [];

export default (state = defaultState, action) => {
    let mi, car, past, future;
    switch (action.type) {
        case 'INITIALIZE_CARS':
            return action.cars;
        case 'ADD_CAR':
            return [...state, action.car];
        case 'REMOVE_CAR':
            return state.filter(c => c.id !== action.carId);
        case 'EDIT_CAR':
            return state.map(c => {
                if (c.id === action.updatedCar.id) {
                    return { ...c, ...action.updatedCar };
                }
                return c;
            });
        case 'DELETE_CAR':
            return state.filter(c => c.id !== action.carId);
        case 'ADD_SHARED_CAR_USER':
            return state.map(c => {
                if (c.id === action.sharedUser.carId) {
                    return {
                        ...c,
                        sharedCars: [...c.sharedCars, action.sharedUser]
                    };
                }
                return c;
            });
        case 'DELETE_SHARED_CAR_USER':
            return state.map(c => {
                if (c.id === Number(action.carId)) {
                    return {
                        ...c,
                        sharedCars: c.sharedCars.filter(
                            sc => sc.email !== action.email
                        )
                    };
                }
                return c;
            });
        case 'CLEAR_CARS':
            return defaultState;
        case 'SET_CAR_MAINTENANCE':
            // when we get the maintenance for a specific car, we want to update that in the main list to handle order changes, additions, and updates
            return state.map(c => {
                if (c.id === action.carId) {
                    return {
                        ...c,
                        pastMaintenance:
                            action.maintenanceItems.pastMaintenance,
                        futureMaintenance:
                            action.maintenanceItems.futureMaintenance
                    };
                }
                return c;
            });
        case 'DELETE_MAINTENANCE_ITEM':
            car = state.find(c => c.id === action.carId);
            // remove item from either list
            const pastMaintenance = car.pastMaintenance.filter(
                item => item.id !== action.maintenanceId
            );
            const futureMaintenance = car.futureMaintenance.filter(
                item => item.id !== action.maintenanceId
            );
            return state.map(c => {
                if (c.id === car.id) {
                    return { ...c, pastMaintenance, futureMaintenance };
                }
                return c;
            });
        default:
            return state;
    }
};
