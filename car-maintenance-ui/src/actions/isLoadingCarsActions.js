export const setIsLoadingCars = isLoadingCars => ({
    type: 'SET_IS_LOADING_CARS',
    isLoadingCars
});
