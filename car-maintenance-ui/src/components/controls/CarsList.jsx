import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Grid, Segment, Header, Icon, Button } from 'semantic-ui-react';

import CarListItem from './CarListItem';
import NewCarModal from './NewCarModal';
import { initializeCarsAsync } from '../../actions/carActions';
import { bodyFadeStart, bodyFadeEnd } from '../../misc/transitionHelpers';

class CarsList extends React.Component {
    static propTypes = {
        cars: PropTypes.array,
        isAuthenticated: PropTypes.bool,
        isLoadingCars: PropTypes.bool,
        initializeCars: PropTypes.func
    };

    static defaultProps = {
        cars: []
    };

    constructor(props) {
        super(props);
        this.state = {
            newCarModalOpen: false,
            newCarModalClass: ''
        };
    }

    componentDidMount() {
        this.getCars();
    }

    componentDidUpdate(prevProps) {
        if (!prevProps.isAuthenticated && this.props.isAuthenticated) {
            this.getCars();
        }
    }

    getCars = () => {
        if (
            this.props.isAuthenticated &&
            this.props.cars.length === 0 &&
            !this.props.isLoadingCars
        ) {
            this.props.initializeCars();
        }
    };

    toggleNewCarModal = () => {
        const timeout = 250;
        if (this.state.newCarModalOpen) {
            this.setState(() => ({ newCarModalClass: '' }));
            bodyFadeEnd(timeout);
            setTimeout(() => {
                this.setState(() => ({ newCarModalOpen: false }));
            }, timeout);
        } else {
            this.setState(() => ({
                newCarModalOpen: true
            }));
            setTimeout(() => {
                this.setState(() => ({ newCarModalClass: 'slide' }));
                bodyFadeStart();
            }, 1);
        }
    };

    render() {
        return (
            <>
                <h1>My Cars</h1>
                <Grid container columns="equal" className="car-list">
                    {Array.isArray(this.props.cars) &&
                        this.props.cars.map(car => (
                            <CarListItem key={car.id} car={car} />
                        ))}
                    <Grid.Row>
                        <Grid.Column>
                            <Segment raised className="no-cars">
                                <div className="flexbox-center">
                                    <Header icon>
                                        <Icon name="car" />
                                    </Header>
                                </div>
                                <Grid centered>
                                    <Grid.Column
                                        computer={6}
                                        tablet={8}
                                        mobile={16}
                                    >
                                        <Button
                                            fluid
                                            animated="fade"
                                            color="blue"
                                            className="round-button"
                                            onClick={this.toggleNewCarModal}
                                        >
                                            <Button.Content visible>
                                                Add a car
                                            </Button.Content>
                                            <Button.Content hidden>
                                                <Icon name="plus" />
                                            </Button.Content>
                                        </Button>
                                    </Grid.Column>
                                </Grid>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <NewCarModal
                    open={this.state.newCarModalOpen}
                    toggleOpen={this.toggleNewCarModal}
                    newCarModalClass={this.state.newCarModalClass}
                />
            </>
        );
    }
}

const mapStateToProps = ({ cars, auth, isLoadingCars }) => {
    return {
        cars,
        isAuthenticated: !!auth.accessToken && auth.userExists,
        isLoadingCars
    };
};
const mapDispatchToProps = dispatch => ({
    initializeCars: () => dispatch(initializeCarsAsync())
});

export default connect(mapStateToProps, mapDispatchToProps)(CarsList);
