export const { GOOGLE_CLIENT_ID } = process.env;

export const deviceWidths = {
    mobile: 320,
    tablet: 768,
    computer: 992,
    largeMonitor: 1280,
    wideMonitor: 1920
};

export const DATE_PARSE_FORMAT = 'YYYY-MM-DDTHH:mm:ss.SSS';
export const DATE_FORMAT_STRING = 'MMM D, YYYY';
export const DATE_SIMPLE_STRING = 'YYYY-MM-DD';
