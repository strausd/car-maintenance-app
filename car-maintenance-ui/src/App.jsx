import React from 'react';
import { Provider } from 'react-redux';

import AppRouter from './routers/AppRouter';
import store from './store/store';

import 'semantic-ui-css/semantic.min.css';
import './styles/App.scss';

const App = () => (
    <div className="cm-app">
        <Provider store={store}>
            <AppRouter />
        </Provider>
    </div>
);

export default App;
