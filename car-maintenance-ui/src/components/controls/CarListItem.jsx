import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Grid, Segment, Divider, Header, Icon } from 'semantic-ui-react';
import { reverse } from 'named-urls';

import CarListMaintenanceTable from './CarListMaintenanceTable';
import routes from '../../routers/routes';
import CarDetailsTable from './CarDetailsTable';

const CarListItem = ({ car, history }) => {
    const navigateToCarDetail = () => {
        history.push(reverse(routes.carDetail, { id: car.id }));
    };

    return (
        <Grid.Row>
            <Grid.Column>
                <Segment raised>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column width={16}>
                                <Divider horizontal>
                                    <Header
                                        as="h4"
                                        onClick={navigateToCarDetail}
                                        className="clickable"
                                    >
                                        <Icon name="car" />
                                        Details
                                    </Header>
                                </Divider>
                            </Grid.Column>
                        </Grid.Row>
                        <CarDetailsTable car={car} showDetailsButton />
                        <Grid.Row>
                            <Grid.Column width={16}>
                                <Divider horizontal>
                                    <Header as="h4">
                                        <Icon name="wrench" />
                                        Upcoming Maintenance
                                    </Header>
                                </Divider>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row className="no-top-padding">
                            <Grid.Column
                                width={16}
                                className="maintenance-container"
                            >
                                <CarListMaintenanceTable
                                    maintenanceItems={car.futureMaintenance}
                                    carId={car.id}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Segment>
            </Grid.Column>
        </Grid.Row>
    );
};

CarListItem.propTypes = {
    car: PropTypes.object,
    history: PropTypes.object
};

export default withRouter(CarListItem);
