SET TIME ZONE 'UTC';

CREATE TABLE users (
  id serial PRIMARY KEY,
  first_name varchar(50),
  last_name varchar(50),
  email varchar(75) UNIQUE,
  is_admin boolean,
  created_at timestamp,
  updated_at timestamp
);

CREATE TABLE cars (
  id serial PRIMARY KEY,
  make varchar(50) NOT NULL,
  model varchar(50) NOT NULL,
  year integer NOT NULL,
  color varchar(20) NOT NULL,
  nickname varchar(50),
  miles integer,
  owner_id integer REFERENCES users (id),
  created_at timestamp,
  updated_at timestamp
);

CREATE TABLE maintenance_items (
  id serial PRIMARY KEY,
  title varchar(100) NOT NULL,
  description varchar(1000),
  target_date timestamp,
  target_miles integer,
  is_complete boolean,
  car_id integer REFERENCES cars(id),
  created_at timestamp,
  updated_at timestamp
);

---------------------------------------------------

ALTER TABLE maintenance_items
  ADD COLUMN completed_date timestamp,
  ADD COLUMN completed_miles integer;

---------------------------------------------------

BEGIN;

ALTER TABLE cars
  DROP CONSTRAINT cars_owner_id_fkey;

ALTER TABLE cars
  ADD CONSTRAINT cars_owner_id_fkey
  FOREIGN KEY (owner_id)
  REFERENCES users (id)
  ON DELETE CASCADE;

ALTER TABLE maintenance_items
  DROP CONSTRAINT maintenance_items_car_id_fkey;

ALTER TABLE maintenance_items
  ADD CONSTRAINT maintenance_items_car_id_fkey
  FOREIGN KEY (car_id)
  REFERENCES cars (id)
  ON DELETE CASCADE;

COMMIT;

---------------------------------------------------

ALTER TABLE cars
  ADD COLUMN mileage integer;

---------------------------------------------------

ALTER TABLE users
  DROP COLUMN IF EXISTS first_name,
  DROP COLUMN IF EXISTS last_name;

---------------------------------------------------

CREATE TABLE shared_cars (
  car_id integer NOT NULL,
  email varchar(75) NOT NULL,
  created_at timestamp,
  updated_at timestamp,
  CONSTRAINT fk_car_shared_car
    FOREIGN KEY (car_id)
      REFERENCES cars(id),
  PRIMARY KEY(car_id, email)
);
