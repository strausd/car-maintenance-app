import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    Grid,
    Segment,
    Divider,
    Header,
    Icon,
    Button
} from 'semantic-ui-react';

import { initializeCarsAsync, deleteCarAsync } from '../../actions/carActions';
import routes from '../../routers/routes';
import CarListMaintenanceTable from '../controls/CarListMaintenanceTable';
import CarDetailsTable from '../controls/CarDetailsTable';

class CarDetailPage extends React.Component {
    static propTypes = {
        history: PropTypes.object,
        initializeCars: PropTypes.func,
        deleteCar: PropTypes.func,
        car: PropTypes.object,
        isAuthenticated: PropTypes.bool,
        isLoadingCars: PropTypes.bool,
        isOwner: PropTypes.bool
    };

    componentDidMount() {
        // only initialize if we don't have a car and aren't trying to get one already
        if (!this.props.car.id && !this.props.isLoadingCars) {
            this.props.initializeCars();
        }
    }

    componentDidUpdate(prevProps) {
        if (
            prevProps.isAuthenticated !== this.props.isAuthenticated &&
            !this.props.car.id &&
            !this.props.isLoadingCars
        ) {
            this.props.initializeCars();
        }
    }

    handleDeleteCar = () => {
        this.props.deleteCar(this.props.car.id);
        this.props.history.push(routes.root);
    };

    render() {
        return (
            <div className="page home">
                <Grid container columns="equal" className="car-list">
                    <Grid.Row>
                        <Grid.Column>
                            <Segment raised>
                                <Grid>
                                    <Grid.Row>
                                        <Grid.Column width={16}>
                                            <Divider horizontal>
                                                <Header as="h4">
                                                    <Icon name="car" />
                                                    Details
                                                </Header>
                                            </Divider>
                                        </Grid.Column>
                                    </Grid.Row>
                                    <CarDetailsTable car={this.props.car} />
                                    <Grid.Row>
                                        <Grid.Column width={16}>
                                            <Divider horizontal>
                                                <Header as="h4">
                                                    <Icon name="wrench" />
                                                    Upcoming Maintenance
                                                </Header>
                                            </Divider>
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row className="no-top-padding">
                                        <Grid.Column
                                            width={16}
                                            className="maintenance-container"
                                        >
                                            <CarListMaintenanceTable
                                                maintenanceItems={
                                                    this.props.car
                                                        .futureMaintenance
                                                }
                                                carId={this.props.car.id}
                                            />
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column width={16}>
                                            <Divider horizontal>
                                                <Header as="h4">
                                                    <Icon name="check" />
                                                    Completed Maintenance
                                                </Header>
                                            </Divider>
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row className="no-top-padding">
                                        <Grid.Column
                                            width={16}
                                            className="maintenance-container"
                                        >
                                            <CarListMaintenanceTable
                                                maintenanceItems={
                                                    this.props.car
                                                        .pastMaintenance
                                                }
                                                carId={this.props.car.id}
                                                past
                                            />
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Divider />
                                    {this.props.isOwner && (
                                        <Grid.Row centered>
                                            <Grid.Column
                                                mobile={16}
                                                tablet={8}
                                                computer={6}
                                            >
                                                <Button
                                                    fluid
                                                    animated="fade"
                                                    color="red"
                                                    className="round-button"
                                                    onClick={
                                                        this.handleDeleteCar
                                                    }
                                                >
                                                    <Button.Content visible>
                                                        Delete this car
                                                    </Button.Content>
                                                    <Button.Content hidden>
                                                        <Icon name="times" />
                                                    </Button.Content>
                                                </Button>
                                            </Grid.Column>
                                        </Grid.Row>
                                    )}
                                </Grid>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        );
    }
}

const mapStateToProps = ({ cars, auth, isLoadingCars }, ownProps) => {
    const carId = Number(ownProps.match.params.id);
    const car = cars.find(c => c.id === carId) || {};
    return {
        car,
        isAuthenticated: !!auth.accessToken,
        isLoadingCars,
        isOwner: auth.userId === car.ownerId
    };
};

const mapDispatchToProps = dispatch => ({
    initializeCars: () => dispatch(initializeCarsAsync()),
    deleteCar: id => dispatch(deleteCarAsync(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(CarDetailPage);
