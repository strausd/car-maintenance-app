const express = require('express');

const carService = require('../service/carService');

const router = express.Router();

router.get('/car', (req, res) => {
    carService.getAllCarsForUser(req, res);
});
router.post('/car', (req, res) => {
    carService.createCar(req, res);
});
router.get('/car/:id', (req, res) => {
    carService.getCar(req, res);
});
router.patch('/car/:id', (req, res) => {
    carService.updateCar(req, res);
});
router.delete('/car/:id', (req, res) => {
    carService.deleteCar(req, res);
});
router.get('/car/:id/share', (req, res) => {
    carService.getSharedOwners(req, res);
});
router.post('/car/:id/share', (req, res) => {
    carService.addSharedOwner(req, res);
});
router.delete('/car/:id/share', (req, res) => {
    carService.deleteSharedOwner(req, res);
});

module.exports = router;
