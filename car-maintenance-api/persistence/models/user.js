const { DataTypes } = require('sequelize');

const UserModel = sequelizeInstance => {
    return sequelizeInstance.define('users', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: 'email'
        },
        isAdmin: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            field: 'is_admin',
            defaultValue: false
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: false,
            field: 'updated_at'
        }
    });
};

module.exports = UserModel;
