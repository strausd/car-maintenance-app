import api from '../apiControllers/api';
import { setIsLoadingCars } from './isLoadingCarsActions';

export const initializeCars = cars => ({
    type: 'INITIALIZE_CARS',
    cars
});

export const initializeCarsAsync = () => {
    return dispatch => {
        dispatch(setIsLoadingCars(true));
        api.getAllCarsForUser()
            .then(cars => {
                dispatch(initializeCars(cars));
                dispatch(setIsLoadingCars(false));
            })
            .catch(e => console.error(e));
    };
};

export const clearCars = () => ({ type: 'CLEAR_CARS' });

export const addCar = car => ({
    type: 'ADD_CAR',
    car
});

export const addCarAsync = carData => {
    return dispatch => {
        api.createCar(carData)
            .then(car => {
                dispatch(addCar(car));
            })
            .catch(e => console.error(e));
    };
};

export const removeCar = carId => ({
    type: 'REMOVE_CAR',
    carId
});

export const editCar = updatedCar => ({
    type: 'EDIT_CAR',
    updatedCar
});

export const editCarAsync = (carId, carDetails) => {
    return dispatch => {
        api.updateCar(carId, carDetails)
            .then(car => {
                dispatch(editCar(car));
            })
            .catch(e => console.error(e));
    };
};

export const deleteCar = carId => ({
    type: 'DELETE_CAR',
    carId
});

export const deleteCarAsync = carId => {
    return dispatch => {
        api.deleteCar(carId)
            .then(() => {
                dispatch(deleteCar(carId));
            })
            .catch(e => console.error(e));
    };
};

export const addSharedCarUser = sharedUser => ({
    type: 'ADD_SHARED_CAR_USER',
    sharedUser
});

export const addSharedCarUserAsync = (carId, email) => {
    return dispatch => {
        api.addSharedCarUser(carId, email)
            .then(sharedUser => {
                dispatch(addSharedCarUser(sharedUser));
            })
            .catch(e => console.error(e));
    };
};

export const deleteSharedCarUser = (carId, email) => ({
    type: 'DELETE_SHARED_CAR_USER',
    carId,
    email
});

export const deleteSharedCarUserAsync = (carId, email) => {
    return dispatch => {
        api.deleteSharedCarUser(carId, email)
            .then(response => {
                if (response) {
                    dispatch(deleteSharedCarUser(carId, email));
                }
            })
            .catch(e => console.error(e));
    };
};

export const setCarMaintenance = (maintenanceItems, carId) => ({
    type: 'SET_CAR_MAINTENANCE',
    maintenanceItems,
    carId
});

export const getCarMaintenanceItemsAsync = id => {
    return dispatch => {
        api.getCarMaintenanceItems(id)
            .then(maintenanceItems => {
                dispatch(setCarMaintenance(maintenanceItems, id));
            })
            .catch(e => console.error(e));
    };
};

export const addMaintenenaceItemAsync = (carId, maintenanceItemData) => {
    return dispatch => {
        api.createMaintenanceItem(carId, maintenanceItemData)
            .then(() => {
                return api.getCarMaintenanceItems(carId);
            })
            .then(maintenanceItems => {
                dispatch(setCarMaintenance(maintenanceItems, carId));
            })
            .catch(e => console.error(e));
    };
};

export const updateMaintenanceItemAsync = (
    carId,
    maintenanceId,
    maintenanceItem
) => {
    return dispatch => {
        api.updateMaintenanceItem(carId, maintenanceId, maintenanceItem)
            .then(() => {
                return api.getCarMaintenanceItems(carId);
            })
            .then(maintenanceItems => {
                dispatch(setCarMaintenance(maintenanceItems, carId));
            })
            .catch(e => console.error(e));
    };
};

export const deleteMaintenanceItem = (carId, maintenanceId) => ({
    type: 'DELETE_MAINTENANCE_ITEM',
    carId,
    maintenanceId
});

export const deleteMaintenanceItemAsync = (carId, maintenanceId) => {
    return dispatch => {
        api.deleteMaintenanceItem(carId, maintenanceId)
            .then(() => {
                dispatch(deleteMaintenanceItem(carId, maintenanceId));
            })
            .catch(e => console.error(e));
    };
};
