import React from 'react';
import { Icon } from 'semantic-ui-react';

const HomeLogin = () => (
    <div className="home-login">
        <h1>New here? Go ahead and login</h1>
        <div className="icon-holder">
            <Icon name="arrow up" />
        </div>
    </div>
);

export default HomeLogin;
