import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'semantic-ui-react';

import dayjs from '../../misc/dayjs';
import MaintenanceDetailModal from './MaintenanceDetailModal';
import { addCommasToNumbers } from '../../misc/util';
import { DATE_PARSE_FORMAT, DATE_FORMAT_STRING } from '../../misc/constants';

class CarListMaintenanceTableItem extends React.Component {
    static propTypes = {
        item: PropTypes.object
    };

    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false
        };
    }

    completedDate = dayjs.utc(this.props.item.completedDate, DATE_PARSE_FORMAT);

    targetDate = dayjs.utc(this.props.item.targetDate, DATE_PARSE_FORMAT);

    toggleModal = () => {
        this.setState(prevState => ({ isModalOpen: !prevState.isModalOpen }));
    };

    render() {
        let date, parsedDate, miles;
        if (this.props.item.isComplete) {
            date = this.props.item.completedDate;
            parsedDate = this.completedDate;
            miles = this.props.item.completedMiles;
        } else {
            date = this.props.item.targetDate;
            parsedDate = this.targetDate;
            miles = this.props.item.targetMiles;
        }
        return (
            <>
                <Table.Row
                    onClick={this.toggleModal}
                    className={`clickable ${this.props.item.priority}`}
                >
                    <Table.Cell>{this.props.item.title}</Table.Cell>
                    <Table.Cell>
                        {date ? parsedDate.format(DATE_FORMAT_STRING) : 'N/A'}
                    </Table.Cell>
                    <Table.Cell>
                        {addCommasToNumbers(miles) || 'N/A'}
                    </Table.Cell>
                </Table.Row>
                <MaintenanceDetailModal
                    open={this.state.isModalOpen}
                    onClose={this.toggleModal}
                    item={this.props.item}
                />
            </>
        );
    }
}

export default CarListMaintenanceTableItem;
