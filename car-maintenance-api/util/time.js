const dayjs = require('./dayjs');

// ignore timezone when initially setting it in UTC
const DATE_PARSE_FORMAT = 'YYYY-MM-DDTHH:mm:ss.SSS';
// add timezone back in after it has already been cleared and set to UTC
const TARGET_DATE_FORMAT = 'YYYY-MM-DDTHH:mm:ss.SSSZ';

const validateDateTime = dateStr => {
    const dateTime = dayjs.utc(dateStr, DATE_PARSE_FORMAT);
    if (dateTime.isValid()) {
        return dateTime;
    }
    return false;
};

const getDateTimeString = dayjsObj => dayjsObj.format(TARGET_DATE_FORMAT);

module.exports = {
    validateDateTime,
    getDateTimeString,
    DATE_PARSE_FORMAT
};
