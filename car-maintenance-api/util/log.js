const dayjs = require('dayjs');

const targetDateFormat = 'YYYY-MM-DDTHH:mm:ss.SSSZ';

module.exports = {
    log: msg =>
        console.log(`[${dayjs().format(targetDateFormat)}] [LOG] | ${msg}`),
    debug: msg =>
        console.debug(`[${dayjs().format(targetDateFormat)}] [DEBUG] | ${msg}`),
    info: msg =>
        console.info(`[${dayjs().format(targetDateFormat)}] [INFO] | ${msg}`),
    warn: msg =>
        console.warn(`[${dayjs().format(targetDateFormat)}] [WARN] | ${msg}`),
    error: msg =>
        console.error(`[${dayjs().format(targetDateFormat)}] [ERROR] | ${msg}`)
};
