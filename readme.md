## Car Maintenance App
This application is designed to help you track maintenance for your vehicles. Current functionality includes:
- Authentication support with Google
- Manage multiple cars
- Manage past and future maintenance for your vehicles
- Automatic sorting and highlighting of maintenance based on how soon it should be completed
- Share your cars and maintenance schedule with others
- Automated test deployment to Heroku via GitLab CI on any branch
- Automated prod deployment for tagged builds

#### Test Instance
<https://test-car-maintenance.herokuapp.com/>
#### Prod Instance
<https://car-maintenance-strausd.herokuapp.com/>
