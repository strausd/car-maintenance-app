export const bodyFadeStart = () => {
    document.body.classList.add('bodyFade', 'z-10');
};

export const bodyFadeEnd = timeout => {
    document.body.classList.remove('bodyFade');
    setTimeout(() => {
        document.body.classList.remove('z-10');
    }, timeout);
};
