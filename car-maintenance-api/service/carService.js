const { Op } = require('sequelize');

const { CarModel, SharedCarModel } = require('../persistence/sequelize');
const objEmptyStringToNull = require('../util/objEmptyStringToNull');
const {
    organizeCarMaintenanceItems
} = require('../util/organizeCarMaintenanceItems');

const sortCars = (a, b) => {
    // first sort by year as older cars likely need more maintenance and should be displayed first
    if (a.year < b.year) {
        return -1;
    } else if (a.year > b.year) {
        return 1;
    }
    // then if same year, sort by maintenance miles as cars driven more probably need more maintenance and should be displayed first
    if (a.maintenanceMiles > b.maintenanceMiles) {
        return -1;
    } else if (a.maintenanceMiles < b.maintenanceMiles) {
        return 1;
    }
    // lastly sort by created date
    if (a.createdAt < b.createdAt) {
        return -1;
    }
    if (a.createdAt > b.createdAt) {
        return 1;
    }
    return 0;
};

const getAllCarsForUser = (req, res) => {
    CarModel.findAll({
        where: {
            [Op.or]: [
                { ownerId: res.locals.user.id },
                { '$sharedCars.email$': res.locals.user.email }
            ]
        },
        include: ['maintenance_items', 'sharedCars']
    })
        .then(cars => {
            const formattedCars = cars.map(car => {
                return organizeCarMaintenanceItems(car);
            });
            formattedCars.sort(sortCars);
            res.status(200).json(formattedCars);
        })
        .catch(e => {
            return res.status(400).json(e);
        });
};

const getCar = (req, res) => {
    CarModel.findOne({ where: { id: req.params.id } })
        .then(car => {
            return res.status(200).json(car);
        })
        .catch(e => res.status(400).json({ msg: e }));
};

const createCar = (req, res) => {
    const carData = req.body;
    if (carData.make && carData.model && carData.year && carData.color) {
        CarModel.create({ ...carData, ownerId: res.locals.user.id })
            .then(car => {
                res.status(200).json({
                    ...car.toJSON(),
                    maintenance: { past: [], future: [] }
                });
            })
            .catch(e => {
                console.error(e);
                return res.status(400).json({ msg: e });
            });
    } else {
        return res
            .status(400)
            .json({ msg: 'Incomplete car data. Car not created.' });
    }
};

const updateCar = (req, res) => {
    CarModel.findOne({ where: { id: req.params.id } })
        .then(car => {
            return car.update(objEmptyStringToNull(req.body));
        })
        .then(car => res.status(200).json(car))
        .catch(e => res.status(400).json({ msg: e }));
};

const deleteCar = (req, res) => {
    CarModel.findOne({ where: { id: req.params.id } })
        .then(car => {
            if (res.locals.user.isAdmin || car.ownerId === res.locals.user.id) {
                return car.destroy();
            } else {
                return res.status(403).json({ msg: 'Unauthorized' });
            }
        })
        .then(something => res.status(200).json(something))
        .catch(e => res.status(400).json({ msg: e }));
};

const getSharedOwners = (req, res) => {
    SharedCarModel.findAll({
        where: {
            carId: req.params.id
        }
    })
        .then(sharedCarObjs => {
            const sharedWith = sharedCarObjs.map(sharedCar => sharedCar.email);
            res.status(200).json(sharedWith);
        })
        .catch(e => res.status(400).json({ msg: e }));
};

const addSharedOwner = (req, res) => {
    SharedCarModel.create({
        carId: req.params.id,
        email: req.body.email.trim().toLowerCase()
    })
        .then(response => res.status(200).json(response))
        .catch(e => res.status(400).json({ msg: e }));
};

const deleteSharedOwner = (req, res) => {
    SharedCarModel.destroy({
        where: {
            carId: req.params.id,
            email: req.body.email.trim().toLowerCase()
        }
    })
        .then(sharedCar => res.status(200).json(sharedCar))
        .catch(e => res.status(400).json({ msg: e }));
};

module.exports = {
    getAllCarsForUser,
    getCar,
    createCar,
    updateCar,
    deleteCar,
    getSharedOwners,
    addSharedOwner,
    deleteSharedOwner
};
