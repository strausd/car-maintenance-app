const dayjs = require('./dayjs');

const RED = 'red';
const YELLOW = 'yellow';
const DAYS_TO_RED = 1;
const DAYS_TO_YELLOW = 14;

const getPriorityFromDate = mi => {
    const now = dayjs().utc();
    const targetDate = dayjs.utc(mi.targetDate);
    const daysToTarget = dayjs.duration(targetDate.diff(now)).asDays();
    if (daysToTarget <= DAYS_TO_RED) {
        return RED;
    } else if (daysToTarget <= DAYS_TO_YELLOW) {
        return YELLOW;
    }
    return null;
};

// assumes maintenance item has car.maintenanceMileage and car.estimatedDailyMileage
const getPriorityFromMiles = mi => {
    if (
        mi.car.maintenanceMileage >=
        mi.targetMiles - mi.car.estimatedDailyMileage * DAYS_TO_RED
    ) {
        return RED;
    } else if (
        mi.car.maintenanceMileage >=
        mi.targetMiles - mi.car.estimatedDailyMileage * DAYS_TO_YELLOW
    ) {
        return YELLOW;
    }
    return null;
};

// assumes plain javascript object for maintenance item, not sequelize model
const getMaintenanceItemPriority = mi => {
    const priorities = [getPriorityFromDate(mi), getPriorityFromMiles(mi)];
    // set highest priority level on maintenance item
    if (priorities.includes(RED)) {
        mi.priority = RED;
    } else if (priorities.includes(YELLOW)) {
        mi.priority = YELLOW;
    } else {
        mi.priority = '';
    }
    return mi;
};

// assumes maintenance item has car.maintenanceMileage and car.estimatedDailyMileage
// return dayjs object
const estimateDateFromMileage = (mi, isComplete = true) => {
    const now = dayjs().utc();
    let daysFromMaintenance;
    if (isComplete) {
        daysFromMaintenance = Math.round(
            (mi.car.maintenanceMileage - mi.completedMiles) /
                mi.car.estimatedDailyMileage
        );
    } else {
        daysFromMaintenance = Math.round(
            (mi.car.maintenanceMileage - mi.targetMiles) /
                mi.car.estimatedDailyMileage
        );
    }
    return now.subtract(daysFromMaintenance, 'day');
};

const getComparisonDate = (mi, isComplete = true) => {
    if (!isComplete) {
        if (mi.targetDate) {
            return dayjs.utc(mi.targetDate);
        } else if (mi.targetMiles) {
            return estimateDateFromMileage(mi, false);
        }
    }
    if (mi.completedDate) {
        return dayjs.utc(mi.completedDate);
    } else if (mi.completedMiles) {
        return estimateDateFromMileage(mi);
    }
    return dayjs.utc(mi.updatedAt);
};

// For past maintenance
const sortMaintenanceExcludingPriority = (a, b, isComplete = true) => {
    const dateA = getComparisonDate(a, isComplete);
    const dateB = getComparisonDate(b, isComplete);
    // If maintenance is not complete (future maintenance), we want the lowest value maintenance items (ones that need to be done sooner)
    // to be at the top of the list within its own priority
    if (!isComplete) {
        if (dateA.isBefore(dateB)) {
            return -1;
        } else if (dateB.isBefore(dateA)) {
            return 1;
        }
    }
    // sort by lowest (oldest) completed dates being last
    if (dateA.isBefore(dateB)) {
        return 1;
    } else if (dateB.isBefore(dateA)) {
        return -1;
    }
    return 0;
};

// For future maintenance
const sortMaintenanceWithPriority = (a, b) => {
    // first sort by priority
    // have to separate it out like this to handle no priority being set
    if (a.priority === b.priority) {
        // if priority is the same, we should sort differently
        return sortMaintenanceExcludingPriority(a, b, false);
    } else if (a.priority === RED) {
        return -1;
    } else if (b.priority === RED) {
        return 1;
    } else if (a.priority === YELLOW) {
        return -1;
    } else if (b.priority === YELLOW) {
        return 1;
    }
    // sort by target date
    if (a.targetDate < b.targetDate) {
        return -1;
    } else if (a.targetDate > b.targetDate) {
        return 1;
    }
    // sort by target miles
    if (a.targetMiles < b.targetMiles) {
        return -1;
    } else if (a.targetMiles > b.targetMiles) {
        return 1;
    }
    return 0;
};

const organizeCarMaintenanceItems = carObj => {
    const car = carObj.toJSON();
    car.pastMaintenance = [];
    car.futureMaintenance = [];

    car.maintenance_items.forEach(mi => {
        mi.car = mi.car
            ? mi.car
            : {
                  maintenanceMileage: car.maintenanceMileage,
                  estimatedDailyMileage: car.estimatedDailyMileage
              };
        if (mi.isComplete) {
            // do nothing if maintenance is complete
            car.pastMaintenance.push(mi);
        } else {
            // if not complete, get priority for the item and add that to the future maintenance array
            car.futureMaintenance.push(
                getMaintenanceItemPriority(
                    mi,
                    car.maintenanceMileage,
                    car.estimatedDailyMileage
                )
            );
        }
    });
    // delete maintenance_items
    delete car.maintenance_items;
    // sort maintenance
    car.pastMaintenance.sort(sortMaintenanceExcludingPriority);
    car.futureMaintenance.sort(sortMaintenanceWithPriority);
    // return new car object
    return car;
};

module.exports = {
    organizeCarMaintenanceItems,
    getMaintenanceItemPriority
};
