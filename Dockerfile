FROM postgres:latest

# copy all my database scripts into /docker-entrypoint-initdb.d/ for initialization
ADD database /docker-entrypoint-initdb.d
